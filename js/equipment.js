$(document).ready(function(){
    $('.check-size').on('click', function(){
        $(this).toggleClass('checked');
    });
    $('.check-color').on('click', function(){
        $(this).toggleClass('checked');
    });
    $('.collapsing_content-block').on('click', function(){
        $(this).children('.chevron').toggleClass('fa-chevron-down');
        $(this).siblings().toggleClass('hidden-block');
    });
    
})