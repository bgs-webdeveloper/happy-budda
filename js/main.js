$(document).ready(function(){
    /*Slick slider plugin*/
    $('.slider-banner').slick({
      arrows: false,
      dots: true
    });

    if($('.select-category').length > 0){
        $('.select-category').selectmenu();
    }

    //Btn buy
    $('.buy').on('click', function(){
      var countProduct = $('#count-product').text();
      countProduct = parseInt(countProduct) + 1;
      $('#count-product').text(countProduct)
    })

    //History order open or close
    $('.history .order').on('click', function(){
      console.log(this);
      $(this).find('.info-order').toggle();
    })

    /*Error input contact form*/
    $('#cf-btn').on('click', function(){

      //Validation for input with class .required-text
      function required_input_text(){
          var requiredText = $('.required');
          for(var i= 0; i < requiredText.length; i++){
            console.log(requiredText[i]);
            if($(requiredText[i]).val() == ''){
              $(requiredText[i]).siblings('.error').css('visibility', 'visible');
            }else{
              $(requiredText[i]).siblings('.error').css('visibility', 'hidden');
            }
          }
      }
      required_input_text();
    })

    //Counter
    $('.counter .minus').on('click', function(){
        $(this).parent().attr('id', 'counter');
        if($('#counter .number').val() > 0){
            var counterMinus = $('#counter .number').val();
            counterMinus = counterMinus - 1;
            $('#counter .number').val(counterMinus)
        }
        $(this).parent().removeAttr('id', 'counter');
    })
    $('.counter .plus').on('click', function(){
        $(this).parent().attr('id', 'counter');
        var counterPlus = $('#counter .number').val();
        counterPlus = parseInt(counterPlus) + 1;
        $('#counter .number').val(counterPlus);
        $(this).parent().removeAttr('id', 'counter');
    })

    //Product cart delete
    $('.product .remove-product').on('click', function(){
        $(this).parent().parent().remove();
    });

    //Discount and promocode in cart
    $('.discont .title, .promocode .title').on('click', function(){
        $(this).siblings().toggle();
        $(this).find('i').toggleClass('rotate');
    });

    //Your-data, delivary and payment in ordering
    $('.your-data .title, .delivary .title, .payment .title').on('click', function(){
        $(this).siblings().toggle();
        $(this).find('i').toggleClass('rotate');
    });

    //Tabs
    if($('#tabs').length > 0){
        $( "#tabs" ).tabs();
    }

    /*Menu humburger*/
    $('#menu-burger').on('click', function(){
        if($('#menu-burger').hasClass('icon-bars')){
            $('#menu-burger').removeClass('icon-bars');
            $('#menu-burger').addClass('icon-times');
            $('#header-menu').show();
        }else{
            $('#menu-burger').removeClass('icon-times');
            $('#menu-burger').addClass('icon-bars');
            $('#header-menu').hide();
        }
    })
})
